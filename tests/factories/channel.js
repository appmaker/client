import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('channel', {
  default: {
    project: '',
    name: '',
    appInfo: '',
    origin: '',
    deployment: '',
    tag: ''
  }
});
