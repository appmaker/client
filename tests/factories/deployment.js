import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('deployment', {
  default: {
    params: {},
    type: FactoryGuy.belongsTo('deploymentType')
  }
});
