import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('origin', {
  default: {
    params: {
      defaultValue: {}
    },
    src: '', // Should be type 'file'
    type: FactoryGuy.belongsTo('originType')
  }
});
