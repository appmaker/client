import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('app-info', {
  default: {
    name: '',
    description: '',
    iconUrl: ''
  }
});
