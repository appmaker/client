import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('project', {
  default: {
    name: '',
    channels: FactoryGuy.hasMany('channel'),
    production: FactoryGuy.belongsTo('channel'),
    beta: FactoryGuy.belongsTo('channel')
  }
});
