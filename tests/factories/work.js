import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('work', {
  default: {
    progress: 50,
    operation: '',
    resourceUrl: '', // TODO: should be targetURL
    status: 201,
    lastUpdate: new Date(1431446790802)
  },
  finished: {
    progress: 100,
    operation: '',
    resourceUrl: '', // TODO: should be targetURL
    status: 201,
    lastUpdate: new Date(1431446790803)
  }
});
