import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('origin-type', {
  default: {
    name: '',
    description: ''
  }
});
