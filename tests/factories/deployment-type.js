import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('deployment-type', {
  default: {
    name: '',
    description: ''
  }
});
