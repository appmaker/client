import Ember from 'ember';
import AdditionalRouteModelsMixin from '../../../mixins/additional-route-models';
import { module, test } from 'qunit';

module('AdditionalRouteModelsMixin');

// Replace this with your real tests.
test('it works', function(assert) {
  var AdditionalRouteModelsObject = Ember.Object.extend(AdditionalRouteModelsMixin);
  var subject = AdditionalRouteModelsObject.create();
  assert.ok(subject);
});
