import Ember from 'ember';
import CurrentControllerMixin from '../../../mixins/current-controller';
import { module, test } from 'qunit';

module('Unit | Mixin | current controller');

// Replace this with your real tests.
test('it works', function(assert) {
  var CurrentControllerObject = Ember.Object.extend(CurrentControllerMixin);
  var subject = CurrentControllerObject.create();
  assert.ok(subject);
});
