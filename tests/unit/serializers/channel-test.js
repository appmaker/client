import {
  moduleForModel,
  test
} from 'ember-qunit';

moduleForModel('channel', {
  // Specify the other units that are required for this test.
  needs: [
    'model:project',
    'model:appInfo',
    'model:origin',
    'model:originType',
    'model:deployment',
    'model:deploymentType'
  ]
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  var record = this.subject();

  var serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
