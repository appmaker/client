import {
  moduleForModel,
  test
} from 'ember-qunit';

moduleForModel('channel', {
  // Specify the other units that are required for this test.
  needs: [
    'model:project',
    'model:appInfo',
    'model:origin',
    'model:originType',
    'model:deployment',
    'model:deploymentType'
  ]
});

test('it exists', function(assert) {
  var model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
