import {
  moduleForModel,
  test
} from 'ember-qunit';

moduleForModel('deployment', {
  // Specify the other units that are required for this test.
  needs: ['model:deploymentType']
});

test('it exists', function(assert) {
  var model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
