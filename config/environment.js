/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'appmaker-client',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    ENV.deploymentHost = 'localhost:4000';
    ENV.apiHost = 'http://localhost:4000';
//     ENV.APP.LOG_RESOLVER = true;
//     ENV.APP.LOG_ACTIVE_GENERATION = true;
//     ENV.APP.LOG_TRANSITIONS = true;
//     ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
//     ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
    ENV.deploymentHost = 'appmaker.tid.ovh:8080';
    ENV.apiHost = 'http://appmaker.tid.ovh:8080';

  }

  ENV.contentSecurityPolicyHeader = 'Content-Security-Policy';
  ENV.contentSecurityPolicy = {
    'script-src': "'self' 'unsafe-eval' http://*:35729 http://*:8080 http://*:4000",
    'font-src': "'self' http://fonts.gstatic.com",
    'connect-src': "'self' *",
    'img-src': "'self' *",
    'style-src': "'self' 'unsafe-inline' http://fonts.googleapis.com",
    'object-src': "'self' http://*.userreport.com https://*.userreport.com",
    'frame-src': "'self' http://*.userreport.com https://*.userreport.com"
  };

  ENV.APP.installedLanguages = ['en', 'es'];
  ENV.APP.languageByDefault = 'en';

  var userReportTrackingID = {
    dev: 'a17a880a-e6fa-4093-b1f1-77baaf64eb1c', // Development environment
    test: '540ec5a3-5fd6-40c0-a7b9-22dd7970cde5', // Testing environment
    user: 'f05216aa-5080-4e1e-813f-b7c1e7b1e91e' // User testing site
  };

  var statisticsTrackingID = {
    dev: 'UA-64312125-1', // Development environment
    test: 'UA-64312125-2', // Testing environment
    user: 'UA-64312125-3' // User testing site
  };

  var trackingType = 'dev';
  if ('TRACKING_TYPE' in process.env) {
    trackingType = process.env.TRACKING_TYPE;
  }

  ENV.APP.userReport = {
    url: 'cdn.userreport.com/userreport.js',
    code: userReportTrackingID[trackingType] || userReportTrackingID.dev
  };

  ENV.APP.statistics = {
    url: 'www.google-analytics.com/analytics.js',
    code: statisticsTrackingID[trackingType] || statisticsTrackingID.dev
  };

  ENV['simple-auth'] = {
    authorizer: 'authorizer:app-maker',
    routeAfterAuthentication: 'projects',
    crossOriginWhitelist: [ENV.apiHost]
  };

  return ENV;
};
