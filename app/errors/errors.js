import Ember from 'ember';

var AppMakerError = Ember.Object.extend({
  name: 'AppMakerError',

  handler: 'handleError',

  init(params) {
    params = params || {};
    Object.keys(params).forEach(key => this.set(key, params[key]));
  },

  handle(target) {
    var handlerMethod = target.get(this.get('handler'));
    if (handlerMethod && handlerMethod.call) {
      handlerMethod.call(target, this);
    } else {
      console.warn('No', this.get('handler'), 'defined!');
    }
  }
});

var ClientError = AppMakerError.extend({
  name: 'ClientError',

  handler: 'handleClientError'
});

/*
 * This error happens when the time we want to wait before the UI inform that
 * it's so much time for the user to be waiting exceeds.
 */
var TaskTimeoutError = ClientError.extend({
  name: 'TaskTimeoutError'
});

var ServerError = AppMakerError.extend({
  name: 'ServerError',

  handler: 'handleServerError',

  init(params) {
    this._super(params);
    this.set('message', this.get('responseText'));
  }
});

export default {
  AppMakerError: AppMakerError,
  ClientError: ClientError,
  TaskTimeoutError: TaskTimeoutError,
  ServerError: ServerError
};
