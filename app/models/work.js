import DS from 'ember-data';

var Work = DS.Model.extend( {
  progress: DS.attr('number'),
  operation: DS.attr('string'),
  resourceUrl: DS.attr('string'), // TODO: should be targetURL
  status: DS.attr('number'),
  lastUpdate: DS.attr('date')
});

export default Work;
