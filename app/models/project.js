import DS from 'ember-data';

var project = DS.Model.extend({
  name: DS.attr('string'),

  creationTime: DS.attr('date'),

  channels: DS.hasMany('channel'),

  findChannelByTag(tag) {
    return this.get('channels').findBy('tag', tag);
  },

  production: function() {
    return this.findChannelByTag('production');
  }.property('channels.@each.tag'),

  beta: function() {
    return this.findChannelByTag('beta');
  }.property('channels.@each.tag')
});

export default project;
