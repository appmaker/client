import DS from 'ember-data';
import Ember from 'ember';

var deploymentType = DS.Model.extend({
  name: DS.attr('string'),
  description: DS.attr('string'),
  translatedDescription: function() {
    return Ember.I18n.t('channel.deployment.type.' + this.get('id'));
  }.property('id')
});

export default deploymentType;
