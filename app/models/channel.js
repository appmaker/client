import DS from 'ember-data';

var channel = DS.Model.extend({
  project: DS.belongsTo('project', { inverse: 'channels' }),
  name: DS.attr('string'),
  appInfo: DS.belongsTo('appInfo'),
  origin: DS.belongsTo('origin', {async: true}),
  deployment: DS.belongsTo('deployment'),
  tag: DS.attr('string')
});

export default channel;
