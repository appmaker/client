import DS from 'ember-data';
import Ember from 'ember';

var deployment = DS.Model.extend({
  params: DS.attr({ defaultValue: Ember.Object.create({}) }),
  type: DS.belongsTo('deploymentType')
});

export default deployment;
