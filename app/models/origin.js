import Ember from 'ember';
import DS from 'ember-data';

var origin = DS.Model.extend({
  params: DS.attr({ defaultValue: () => Ember.Object.create({}) }),
  src: DS.attr('file'),
  type: DS.belongsTo('originType')
});

export default origin;
