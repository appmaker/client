import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  description: DS.attr('string'),
  // TODO: https://gitlab.com/appmaker/server/issues/12
  // icon: DS.attr('file'),
  iconUrl: DS.attr('string')
});
