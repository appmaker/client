import Ember from 'ember';
import DS from 'ember-data';
import ApplicationSerializer from './application';

export default ApplicationSerializer.extend(DS.EmbeddedRecordsMixin, {
  attrs: {
    appInfo: { embedded: 'always' },
    deployment: { embedded: 'always' }
  },

  /**
   * Before normalizing, emulate unique ids from the server with the server
   * format. This way we can use the EmbeddedRecordsMixin to serialize and,
   * most important, to deserialize.
   */
  normalize(type, hash, key) {
    var id = hash.channelId;
    // No appInfo after deleting record
    if (!Ember.isNone(hash.appInfo)) {
      this.addId(this.serverFormatId('appInfo'), id, hash.appInfo);
    }
    // No deployment after deleting record
    if (!Ember.isNone(hash.deployment)) {
      this.addId(this.serverFormatId('deployment'), id, hash.deployment);
    }
    return this._super(type, hash, key);
  },

  // TODO: Extract into a mixin if we use this pattern more.
  attrMap: {
    'project': 'projectId',
    'origin': 'originId'
  },

  keyForAttribute(attr, method) {
    return this.get('attrMap')[attr] || this._super(attr, method);
  },

  keyForRelationship(attr, relationship, method) {
    return this.keyForAttribute(attr, method);
  },

  /**
   * Create a unique id for a embedded record.
   * TODO: Convert into a customization of the EmbeddedRecordsMixin
   * if we are repeating this pattern.
   */
  addId(idName, id, child) {
    if (Array.isArray(child)) {
      child.forEach((item, index) => item[idName] = id + ':' + index);
    }
    else {
      child[idName] = id;
    }
  }
});
