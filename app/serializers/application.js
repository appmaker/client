import NoRootRESTSerializer from './rest-serializer';

var ApplicationSerializer = NoRootRESTSerializer.extend({

  // XXX: Current Ember.js implementation has a flawn as normalizeId()
  // does not accept the typeClass object so it's impossible to make primaryKey
  // depending on the typeClass. So, injecting before using.

  serialize(snapshot, options) {
    this.set('typeKey', snapshot.typeKey);
    return this._super(snapshot, options);
  },

  normalize(typeClass, hash, prop) {
    this.set('typeKey', typeClass.typeKey);
    return this._super(typeClass, hash, prop);
  },

  primaryKey: function () {
    return this.serverFormatId(this.get('typeKey'));
  }.property('typeKey'),

  serverFormatId(type) {
    return type + 'Id';
  }

});

export default ApplicationSerializer;
