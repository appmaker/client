import DS from 'ember-data';
import Ember from 'ember';

var NoRootRESTSerializer = DS.RESTSerializer.extend({
  serializeIntoHash(hash, type, snapshot, options) {
    Ember.merge(hash, this.serialize(snapshot, options));
  },

  extractSingle(store, type, payload, id) {
    var root = type.typeKey;
    var rootedPayload = { [root]: payload };
    return this._super(store, type, rootedPayload, id);
  },

  extractArray(store, type, payload, id) {
    var root = Ember.String.pluralize(type.typeKey);
    var rootedPayload = { [root]: payload };
    return this._super(store, type, rootedPayload, id);
  }
});

export default NoRootRESTSerializer;
