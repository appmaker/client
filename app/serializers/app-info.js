import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  normalize() {
    return this._super.apply(this, arguments);
  }
});
