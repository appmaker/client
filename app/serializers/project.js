import DS from 'ember-data';
import ApplicationSerializer from './application';

var ProjectSerializer = ApplicationSerializer.extend(DS.EmbeddedRecordsMixin, {
  attrs: {
    channels: { embedded: 'always' }
  }
});

export default ProjectSerializer;
