import Ember from 'ember';
import AdditionalRouteModelsMixin from '../mixins/additional-route-models';
import Statistics from '../statistics/statistics';
import AuthenticatedRouteMixin from
         'simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin,
                                  AdditionalRouteModelsMixin, {
  model: function() {
    return this.store.find('project');
  },
  // now define the other models as a hash of property names to inject
  // onto the controller
  additionalModels: function() {
    return {
      originTypes: this.store.find('originType'),
      deploymentTypes: this.store.find('deploymentType')
    };
  },
  collectStatistics: function(){
    this.send('statistics', new Statistics.ProjectList.Browsing());
  }.on('activate')
});
