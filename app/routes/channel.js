import Ember from 'ember';
import AdditionalRouteModelsMixin from '../mixins/additional-route-models';

var ChannelRoute =  Ember.Route.extend(AdditionalRouteModelsMixin, {
  actions: {
    willTransition() {
      if (!this.get('controller.deleteMode')) {
        this.rollbackModel();
      }
    }
  },

  model: function(params) {
    var channelModel = this.store.find('channel', params.channel_id) ||
                       {};
    return channelModel;
  },

  setupController: function(controller, model) {
    this._super(controller, model);
    this.controller.send('setup');
  },

  rollbackModel() {
    this.get('controller.content.appInfo').rollback();
    // It's the only way to reset the object deployment.params
    // deployment isLoaded but not isDirty, or isSaving
    this.get('controller.content').reload();
  },

  // now define the other models as a hash of property names to inject onto
  // the controller
  additionalModels: function() {
    return {
      originTypes: this.store.all('originType'),
      deploymentTypes: this.store.all('deploymentType')
    };
  }
});

export default ChannelRoute;
