import Ember from 'ember';

import AdditionalRouteModelsMixin from '../../mixins/additional-route-models';

var ChannelNewRoute =  Ember.Route.extend(AdditionalRouteModelsMixin, {
  queryParams: {
    tag: {
      refreshModel: true
    }
  },

  actions: {
    willTransition() {
      if (this._templateChannel.get('isNew')) {
        this.deleteModel();
      }
    },

    //overriding behavior by default because we need to close the modal UI and
    //also navigate to projects route
    acceptModal() {
      this.send('closeModal');
      var channelsNewController = this.controller;
      var savedChannelSuccessfully = channelsNewController.get('savedChannel');
      if (savedChannelSuccessfully) {
        this.transitionTo('channel', channelsNewController.get('model'));
      } else {
        this.transitionTo('projects');
      }
    }
  },

  setupController: function(controller, model) {
    this._super(controller, model);
    this.controller.send('setup');
  },

  model: function(params) {
    var templateChannel = this.store.createRecord('channel');
    templateChannel.set('tag', params.tag);
    templateChannel.set('name', params.name);
    templateChannel.set('project', this.modelFor('project'));
    templateChannel.set('appInfo', this.store.createRecord('appInfo'));
    templateChannel.set('origin', this.store.createRecord('origin'));
    templateChannel.set('deployment', this.store.createRecord('deployment'));
    this._templateChannel = templateChannel;
    return templateChannel;
  },

  deleteModel() {
    var templateChannel = this._templateChannel;
    ['appInfo', 'origin', 'deployment'].forEach(submodelName => {
      var submodel = templateChannel.get(submodelName);
      //XXX: For async relationships such as origin
      if (submodel.isFulfilled) { submodel = submodel.content; }
      this.store.deleteRecord(submodel);
    });
    this.store.deleteRecord(templateChannel);
  },

  additionalModels: function() {
    return {
      originTypes: this.store.all('originType'),
      deploymentTypes: this.store.all('deploymentType')
    };
  }
});

export default ChannelNewRoute;
