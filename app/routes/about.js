import Ember from 'ember';
import Statistics from '../statistics/statistics';

export default Ember.Route.extend({
  collectStatistics: function() {
    this.send('statistics', new Statistics.StartPage.Browsing());
  }.on('activate')
});
