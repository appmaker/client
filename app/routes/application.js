/* globals UserReportManager, StatisticsManager */

import Ember from 'ember';
import config from '../config/environment';
import ApplicationRouteMixin from 'simple-auth/mixins/application-route-mixin';

var ApplicationRoute =  Ember.Route.extend(ApplicationRouteMixin, {
  isLangInstalled(lang) {
    return config.APP.installedLanguages.indexOf(lang) !== -1;
  },

  getLangByDefault() {
    return config.APP.languageByDefault;
  },

  getLang() {
    var langByDefault = this.getLangByDefault();

    var lang = navigator.language ||
      (navigator.userLanguage && navigator.userLanguage.replace(/-[a-z]{2}$/,
      String.prototype.toUpperCase)) || langByDefault;

    if (!this.isLangInstalled(lang)) {
      if (lang.indexOf('-') !== -1) {
        // Removing the country code
        lang = lang.split('-')[0];
        if (!this.isLangInstalled(lang)) {
          lang = langByDefault;
        }
      } else {
        lang = langByDefault;
      }
    }

    return lang;
  },

  init() {
    this._super.apply(this, arguments);
    this.ensureSession();
    StatisticsManager.init(config.APP.statistics);
    UserReportManager.init(config.APP.userReport);
  },

  /*
   * This method ensures the session cookie the *first time* that users
   * access to the service because <AppMakerAuthenticator.restore> method is not
   * executed initially. Basically, the server replies "not authorized" but
   * we will get the session cookie from server and all routes, which requires
   * authentication, will be protected.
   */
  ensureSession() {
    var session = this.get('session');
    if (!session.isAuthenticated) {
      session.authenticate('authenticator:app-maker', {});
    }
  },

  beforeModel() {
    this._super.apply(this, arguments);

    var lang = this.getLang();

    return Ember.$.getJSON('/locales/app.' + lang + '.json').then(data => {
      Ember.I18n.locale = lang;
      Ember.I18n.translations = data;
    });
  },

  actions: {
    openModal(params) {
      this.render(params.template, {
        into: 'application',
        outlet: 'modal',
        model: params.model,
        view: params.view
      });
    },

    closeModal() {
      this.disconnectOutlet({
        outlet: 'modal',
        parentView: 'application'
      });
    },

    //action by default when users click on accept button in the modal view
    acceptModal() {
      this.send('closeModal');
    },

    statistics(event) {
      StatisticsManager.trackEvent(event);
    },

    error(error) {
      var controller = this.get('controller');
      var onErrorMethod = controller.get('onError');
      if (onErrorMethod && onErrorMethod.call) {
        onErrorMethod.call(controller, error);
      }
    }
  }
});

export default ApplicationRoute;
