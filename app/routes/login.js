import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    didTransition() {
      this._super.apply(this, arguments);
      this.set('controller.previousURL', window.location.href);
    }
  }
});
