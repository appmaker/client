/* globals $ */

import Ember from 'ember';

var ProjectsView = Ember.View.extend({
  templateName: 'projects',

  inputNewProjectFocused: true,
  inputNewProjectEvent: 'focus-in',

  scrollTop() {
    $('#projectList').scrollTop(0);
  },

  handlers: {
    onfocus(){
      var inputFocused = !this.get('inputNewProjectFocused');
      this.set('inputNewProjectFocused', inputFocused);

      var inputNewProjectEvent = inputFocused ? 'focus-in' : 'focus-out';
      this.set('inputNewProjectEvent', inputNewProjectEvent);
    }
  },

  didInsertElement() {
    var controller = this.get('controller');
    Object.keys(this.handlers).forEach(eventName => {
      controller.on(eventName, this, this.handlers[eventName]);
    });
    $('#button-view-apps').on('click', this.scrollTop);
  },

  willDestroyElement() {
    var controller = this.get('controller');
    Object.keys(this.handlers).forEach(eventName => {
      controller.off(eventName, this, this.handlers[eventName]);
    });
    $('#button-view-apps').off('click', this.scrollTop);
  }
});

export default ProjectsView;
