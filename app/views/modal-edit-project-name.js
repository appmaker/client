import Ember from 'ember';

export default Ember.View.extend({
  templateName: 'modal-edit-project-name',

  removeModal() {
    var callback = () => {
      this.get('controller').send('closeModal');
    };

    this.$('.tc-modal').one(
      'webkitTransitionEnd otransitionend' +
      'oTransitionEnd msTransitionEnd transitionend',
      callback
    );
    this.$('.tc-modal').removeClass('show');
  },

  didInsertElement() {
    this.$('#editProjectNameField').caretToEnd();

    var controller = this.get('controller');
    controller.on('saved', this, this.removeModal);
  },

  willDestroyElement() {
    var controller = this.get('controller');
    controller.off('saved', this, this.removeModal);
  }
});
