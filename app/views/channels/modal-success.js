import Ember from 'ember';

export default Ember.View.extend({
  templateName: 'modal',
  title: Ember.I18n.t('channels.new.steps.modal.success.title'),
  message: Ember.I18n.t('channels.new.steps.modal.success.message'),
  classTitle: 'positive'
});
