/* globals $ */

import Ember from 'ember';

export default Ember.View.extend({
  templateName: 'channels/new',

  classNames: ['tc-content', 'bg-lighten', 'shadow'],

  attributeBindings: ['columnSpan:column-span'],

  columnSpan: '3 sm1',

  scrollToStep(step) {
    $('#project-screen').animate({
      scrollTop: $('#scroll-anchor-' + step).offset().top
    }, 800);
  },

  handlers: {
    originEnd() {
      this.scrollToStep(2);
    },

    manifestEnd() {
      this.scrollToStep(3);
    },

    resetOrigin() {
      $('.input-file-trigger').each((index, elem) => { elem.value = ''; });
    }
  },

  didInsertElement() {
    var controller = this.get('controller');
    Object.keys(this.handlers).forEach(eventName => {
      controller.on(eventName, this, this.handlers[eventName]);
    });
  },

  willDestroyElement() {
    var controller = this.get('controller');
    Object.keys(this.handlers).forEach(eventName => {
      controller.off(eventName, this, this.handlers[eventName]);
    });
  }
});
