import Ember from 'ember';

export default Ember.View.extend({
  templateName: 'modal',
  title: Ember.I18n.t('modal.tasktimeout.title'),
  message: Ember.I18n.t('modal.tasktimeout.message')
});
