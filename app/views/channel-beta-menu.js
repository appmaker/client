import Ember from 'ember';
import ProjectItemMixin from '../mixins/project-item-mixin';

var ChannelBetaMenuView = Ember.View.extend(ProjectItemMixin, {
  templateName: 'channel/channelItem',
  channel: null,
  title: Ember.I18n.t('channel.types.beta'),
  tag: 'beta'
});

export default ChannelBetaMenuView;
