import Ember from 'ember';

export default Ember.View.extend({

  dragEvents: ['dragover', 'drop'],

  transitionEndEvent: 'webkitTransitionEnd otransitionend' +
        'oTransitionEnd msTransitionEnd transitionend',

  preventDefaultBehavior(e) {
    e.preventDefault();
  },

  handlers: {
    onFeedbackChanged() {
      var feedback = this.get('controller.feedback') === 'false' ? false : true;
      document.body.dataset.feedback = feedback;
      console.info('Feedback is available:', feedback);
    },

    onFeedbackMinimize(callback) {
      if (typeof callback !== 'function') {
        return;
      }

      this.$('#feedback-minimized').on(this.transitionEndEvent,
        () => {
          this.$('#feedback-minimized').off(this.transitionEndEvent);
          callback();
        }
      );
    },

    onAcceptCookies(callback) {
      var cookiesWarnElement = this.$('#cookies-warn');

      if (typeof callback === 'function') {
        cookiesWarnElement.on(this.transitionEndEvent,
          () => {
            cookiesWarnElement.off(this.transitionEndEvent);
            callback();
          }
        );
      }

      cookiesWarnElement.addClass('close');
    }
  },

  didInsertElement() {
    // [OWDAPPMAKE-830] Disable loading dropped files
    this.dragEvents.forEach(eventName => {
      window.addEventListener(eventName, this.preventDefaultBehavior);
    });

    // Initialize feedback button properly because the view is rendered after
    // controller is aware of changes on feedback parameter while it is loading.
    this.handlers.onFeedbackChanged.call(this);
    var controller = this.get('controller');
    Object.keys(this.handlers).forEach(eventName => {
      controller.on(eventName, this, this.handlers[eventName]);
    });
  },

  willDestroyElement() {
    this.dragEvents.forEach(eventName => {
      window.removeEventListener(eventName, this.preventDefaultBehavior);
    });
    var controller = this.get('controller');
    Object.keys(this.handlers).forEach(eventName => {
      controller.on(eventName, this, this.handlers[eventName]);
    });
  }

});
