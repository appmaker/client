import Ember from 'ember';
import ProjectItemMixin from '../mixins/project-item-mixin';

var ChannelProductionMenuView = Ember.View.extend(ProjectItemMixin, {
  templateName: 'channel/channelItem',
  channel: null,
  title: Ember.I18n.t('channel.types.production'),
  tag: 'production'
});

export default ChannelProductionMenuView;
