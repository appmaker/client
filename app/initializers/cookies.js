
export default {
  name: 'cookies',
  after: ['cookie'],
  initialize(container, app) {
    app.inject('controller', 'cookie', 'cookie:main');
  }
};
