import AppMakerAuthenticator from '../authenticators/app-maker';
import Ember from 'ember';

export default {
  name: 'authentication',
  before: 'simple-auth',
  initialize(container, app) {
    Ember.$.ajaxSetup({
      xhrFields: {
         withCredentials: true
      },
      crossDomain: true
    });

    app.register('authenticator:app-maker', AppMakerAuthenticator);
  }
};
