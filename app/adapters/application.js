import config from '../config/environment';
import Ember from 'ember';
import AsyncRESTAdapter from './async-rest-adapter';

var ApplicationAdapter = AsyncRESTAdapter.extend({

  host: config.apiHost,

  namespace: 'appmaker/v1',

  statusModel: 'work',

  pollingInterval: 1000,

  pollingTimeout: 10000,

  pathForType(type) {
    return Ember.String.dasherize(this._super(type));
  }
});

export default ApplicationAdapter;
