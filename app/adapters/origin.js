import ApplicationAdapter from './application';
import FormDataAdapterMixin from 'ember-cli-form-data/mixins/form-data-adapter';

var OriginAdapter = ApplicationAdapter.extend(FormDataAdapterMixin, {
  getFormFields(data) {
    return data;
  },

  getFormKey(key) {
    return key;
  },

  getFormValue(key, value) {
    return value;
  }
});

export default OriginAdapter;
