import AppMakerErrors from '../errors/errors';
import DS from 'ember-data';
import Ember from 'ember';

/**
 * The Poller Mixin for the Async REST adapter provides an implementation
 * of `waitFor()` method using polling.
 *
 * By default, `pollingTimeout` is set to 5s and `pollingInterval` is set
 * to 1s. This means the mixin will check for completion every second until
 * passing 5s. At this time, the model save operation will fail.
 *
 * Customize `pollingTimeout` and `pollingInterval` by model with the
 * typical values for each one.
 */
var AsyncRestPollerMixin = Ember.Mixin.create({

  waitFor(resourceStatus) {
    var expiration = Date.now() + (this.pollingTimeout || 5000);
    return this._waitOrTimeout(resourceStatus, expiration);
  },

  _waitOrTimeout(resourceStatus, expiration) {
    var url = this.urlPrefix(resourceStatus);
    return this.ajax(url, 'GET').then(json => {
      var isExpired = Date.now() > expiration;
      if (isExpired) {
        return Ember.RSVP.reject(this._expiration(url, json));
      }

      var isDone = json.progress === 100;
      if (isDone) {
        var resourceURL = this.urlPrefix(json.$head.headers['Location']);
        return this.ajax(resourceURL, 'GET');
      }

      return this._scheduleNexWait(resourceStatus, expiration);
    });
  },

  _scheduleNexWait(resourceStatus, expiration) {
    var pollingInterval = this.pollingInterval || 1000;
    return new Ember.RSVP.Promise((resolve, reject) => {
      Ember.run.later(this, () => {
        this._waitOrTimeout(resourceStatus, expiration).then(resolve, reject);
      }, pollingInterval);
    });
  },

  _expiration(url, json) {
    var explanation = json.operation + ' (' + json.resourceUrl + ')';
    return new AppMakerErrors.TaskTimeoutError({
      message: 'Timeout while waiting for ' + explanation + ' to complete.\n@' +
                url
    });
  }
});

/**
 * The Async REST adapter extends the basic REST adapter from Ember Data
 * supporting an asynchronous REST model based on Time Bray's
 * [Slow REST](http://www.tbray.org/ongoing/When/200x/2009/07/02/Slow-REST)
 * post.
 *
 * ## The async model
 *
 * The adapter knows an operation is asynchronous because it receives a
 * 202 - Accepted code from the server and a URL in the Location header
 * pointing to a status resource.
 *
 * At this time, the adapter add a status object to the store to allow
 * the application to monitor the progress of the task.
 *
 * Accessing the status resource results in a 200 - Ok status code. In its
 * body, as JSON, there are fields showing the status of the task to be
 * performed:
 *
 *   * `operation` a tag describing the operation to be performed.
 *   * `progress` a number from 0 to 100 describing the percentage of
 *   completion.
 *   * `status` 0 if the task completed with success or an error code.
 *   * `resourceUrl` url for the resource being processed.
 *
 * When `progress` is 100, `status` field is set and the `Location` header
 * points to the same url as `resourceUrl`.
 *
 * ## Saving models
 *
 * When a model is saved by using `Model#save()` the adapter delays the
 * promise returned by `save()` until the task is completed or it rejects
 * after a timeout.
 *
 * ## Configuration
 *
 * Async REST needs some properties and methods to be defined.
 *
 *   * `waitFor` should return a promise for the content of the resource
 *   once it's completed. You can mix `AsyncRestPollerMixin` for an
 *   implementation based on polling.
 *   * `statusModel` is the name of the model holding the status resource.
 *
 * ## Monitoring tasks
 *
 * When an async operation is detected, the adapter adds a creates a new
 * new instance of the model in `statusModel`. All controllers monitoring
 * this model will be updated but the tasks objects won't be updated
 * automatically. Is not the adapter responsibility to update the tasks.
 *
 */
var AsyncRESTAdapter = DS.RESTAdapter.extend(AsyncRestPollerMixin, {

  createRecord(store, type, snapshot) {
    return this._addStatus(store, this._super(store, type, snapshot))
    .then(json => {
      var isAsync = json.$head.status === 202;
      if (!isAsync) { return json; }

      var statusLocation = json.$head.headers['Location'];
      return this.waitFor(statusLocation);
    });
  },

  updateRecord(store, type, snapshot) {
    return this._addStatus(store, this._super(store, type, snapshot))
      .then(json => {
        var isAsync = json.$head.status === 202;
        if (!isAsync) {
          return json;
        }
        var statusLocation = json.$head.headers['Location'];
        return this.waitFor(statusLocation);
      });
  },

  deleteRecord(store, type, snapshot) {
    return this._addStatus(store, this._super(store, type, snapshot));
  },

  _addStatus(store, response) {
    return response.then(json => {
      var isAsyncREST = json.$head.status === 202;
      if (isAsyncREST) {
        this.newStatus(store, json.$head.headers['Location']);
      }
      return json;
    }).catch(e => {
      throw new AppMakerErrors.ServerError(e);
    });
  },

  newStatus(store, statusURL) {
    // TODO: Maybe we should throw upon undefined and ignore if null
    if (!this.statusModel) { return; }

    statusURL = this.urlPrefix(statusURL);
    this.ajax(statusURL, 'GET').then(json => {
      var serializer = store.serializerFor(this.statusModel);
      json = serializer.normalize(store.modelFor(this.statusModel), json);
      store.push(this.statusModel, json);
    });
  },

  // XXX: although this is the recommended way, it sucks to pass the info
  // by using the json but it seems to be proper way for this adapter without
  // refactoring most of it.
  // http://emberjs.com/api/data/classes/DS.RESTAdapter.html#method_ajaxSuccess
  ajaxSuccess(jqXHR, json) {
    json.$head = {
      status: jqXHR.status,
      headers: {
        Location: jqXHR.getResponseHeader('Location')
      }
    };
    return json;
  },

  ajaxError(jqXHR, json) {
    var errorThrown = {
      status: jqXHR.status,
      responseText: jqXHR.responseText
    };

    if (errorThrown.status === 0 && jqXHR.state() === 'rejected') {
      // net::ERR_CONNECTION_REFUSED
      errorThrown.status = 503;
    }

    return new AppMakerErrors.ServerError(errorThrown);
  }
});

export default AsyncRESTAdapter;
