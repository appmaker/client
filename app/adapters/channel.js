import ApplicationAdapter from './application';
import Ember from 'ember';

var ChannelAdapter = ApplicationAdapter.extend({
  buildURL(type, id, snapshot) {
    var url = ['projects', snapshot.get('project').get('id')];
    var channelType = Ember.String.pluralize(type);
    var prefix = this.urlPrefix();

    if (channelType) { url.push(channelType); }
    if (prefix) { url.unshift(prefix); }
    if (id) { url.push(id); }

    return url.join('/');
  }
});

export default ChannelAdapter;
