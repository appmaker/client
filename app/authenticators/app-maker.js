import Ember from 'ember';
import Base from 'simple-auth/authenticators/base';
import config from '../config/environment';

export default Base.extend({
  profileEndpoint: config.apiHost + '/appmaker/v1/profile',

  restore(data) {
    return new Ember.RSVP.Promise((resolve, reject) => {
      Ember.$.ajax({
        url: this.profileEndpoint,
        type: 'GET',
        contentType: 'application/json'
      }).then(response => {
        data = data || {};
        Object.keys(response).forEach(key => data[key] = response[key]);
        resolve(data);
      }, reject);
    });
  },

  authenticate(data) {
    return new Ember.RSVP.Promise((resolve, reject) => {
      return this.restore(data).then(() => {
        Ember.run(() => resolve(data));
      }, () => {
        Ember.run(() => reject());
      });
    });
  },

  invalidate() {
    return new Ember.RSVP.Promise(resolve => {
      Ember.$.ajax({
        url: this.profileEndpoint,
        type: 'DELETE'
      }).always(function() {
        resolve();
      });
    });
  }
});
