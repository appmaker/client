import Ember from 'ember';

/**
 * How to use these Statistics events:
 *
 * 1) Import the module
 *
 *    import Statistics from '../statistics/statistics';
 *
 * 2) Instantiate the class of your event. Some examples:
 *
 *    * Sent on finishing project deletion
 *
 *    var event = new Statistics.Project.Deleted();
 *
 *    console.log('Category: ', event.get('category'));
 *    // Category: Project
 *    console.log('Action: ', event.get('action'));
 *    // Action: Deleted
 *    console.log('Label: ', event.get('label'));
 *    // Label: Project deleted
 *
 *    * Sent on finishing beta channel activation
 *
 *    var event = new Statistics.Channel.Created({
 *      tag: 'beta',
 *      origin: 'zip'
 *    });
 *
 *    console.log('Category: ', event.get('category'));
 *    // Category: Channel
 *    console.log('Action: ', event.get('action'));
 *    // Action: Created
 *    console.log('Label: ', event.get('label'));
 *    // Label: Beta channel created from ZIP to AppMaker
 */

var Action = {};

['Browsing', 'Creating', 'Updating', 'Created', 'Updated', 'Deleted', 'Warn',
 'Fail'].forEach(action => Action[action] = { action: action });

var Category = {};

['StartPage', 'ProjectList', 'Project', 'Channel', 'Notification',
 'Session'].forEach(category => Category[category] = { category: category });

var Stats = Ember.Object.extend({
  init(params) {
    params = params || {};
    Object.keys(params).forEach(key => this.set(key, params[key]));
  }
});

var Project = Stats.extend({
  label: function() {
    return this.get('category') + ' ' + this.get('action').toLowerCase();
  }.property('category', 'action')
});

var Events = {
  StartPage: {
    Browsing: Stats.extend(Category.StartPage, Action.Browsing, {
      label: 'Browsing start page'
    })
  },

  ProjectList: {
    Browsing: Stats.extend(Category.ProjectList, Action.Browsing, {
      label: 'Browsing project list'
    })
  },

  Project: {
    Created: Project.extend(Category.Project, Action.Created),
    Updated: Project.extend(Category.Project, Action.Updated),
    Deleted: Project.extend(Category.Project, Action.Deleted)
  },

  Channel: {
    Creating: Stats.extend(Category.Channel, Action.Creating, {
      label: function() {
        return 'Creating ' + this.get('tag') + ' channel';
      }.property('tag')
    }),
    Created: Stats.extend(Category.Channel, Action.Created, {
      label: function() {
        return this.get('tag').capitalize() + ' channel created from ' +
               this.get('origin').toUpperCase() + ' to AppMaker';
      }.property('tag', 'origin')
    }),
    Updating: Stats.extend(Category.Channel, Action.Updating, {
      label: function() {
        return 'Updating ' + this.get('tag') + ' channel from ' +
                this.get('origin').toUpperCase() + ' to AppMaker';
      }.property('tag', 'origin')
    }),
    Updated: Stats.extend(Category.Channel, Action.Updated, {
      label: function() {
        return this.get('tag').capitalize() + ' channel updated from ' +
               this.get('origin').toUpperCase() + ' to AppMaker';
      }.property('tag', 'origin')
    }),
    Deleted: Stats.extend(Category.Channel, Action.Deleted, {
      label: function() {
        return this.get('tag').capitalize() + ' channel deleted from ' +
               this.get('origin').toUpperCase() + ' to AppMaker';
      }.property('tag', 'origin')
    })
  },

  Notification: {
    Warn: {
      Timeout: Stats.extend(Category.Notification, Action.Warn, {
        label: 'Timeout waiting for completion'
      })
    },
    Fail: {
      Conflict: Stats.extend(Category.Notification, Action.Fail, {
        label: function() {
          return this.get('name').capitalize() + ' in use';
        }.property('name')
      }),
      UnExpected: Stats.extend(Category.Notification, Action.Fail, {
        label: 'Unexpected error'
      })
    }
  }
};

export default Events;
