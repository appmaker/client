import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

export default Router.map(function() {
  this.route('login');
  this.route('about', { path : '/' });
  this.route('tc-components', function() {
    this.route('inputs');
    this.route('buttons');
  });
  this.resource('projects', {path: '/projects'}, function() {
    this.resource('project', { path: ':project_id' }, function() {
      this.route('edit');
      this.resource('channels', {path: 'channels'}, function(){
        this.route('new');
        this.resource('channel', { path: ':channel_id' }, function() {
          this.route('edit');
        });
      });
    });
  });

  // this is our 404 error route
  this.route('not-found', { path: '/*path' });
});
