import Ember from 'ember';

/*
 Usage:

 {{#tc-modal action="action" buttonLabel="string"}}
   {{yield}}
 {{/tc-modal}}
*/

export default Ember.Component.extend({
  buttonLabel: false,

  didInsertElement() {
    Ember.run.next(this, 'addModalShow');
  },
  addModalShow() {
    this.$('.tc-modal').addClass('show');
  },
  removeModalShow(callback) {
    if (typeof callback === 'function') {
      this.$('.tc-modal').one(
        'webkitTransitionEnd otransitionend' +
        'oTransitionEnd msTransitionEnd transitionend',
        callback
      );
    }
    this.$('.tc-modal').removeClass('show');
  },
  actions: {
    action() {
      this.removeModalShow(() => this.sendAction('action'));
    }
  }
});
