import Ember from 'ember';

// Check usage examples in tc-components/inputs

export default Ember.Component.extend({
  tagName: 'label',
  classNameBindings: [
    'switch:tc-switch:tc-checkbox',
    'align',
    'block',
    'disabled'
  ]
});
