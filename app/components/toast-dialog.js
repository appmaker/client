import Ember from 'ember';

/*
 Usage:

 {{#toast-dialog behavior="bottom positive..."
      isShowing=true|false timeout=int}}
    Text for toast
 {{/toast-dialog}}
*/

export default Ember.Component.extend({

  isShowing: false,

  timeout: 1500,

  show: function() {
    if (!this.get('isShowing')) {
      return;
    }

    Ember.run.later(
      this, () => this.set('isShowing', false), this.get('timeout')
    );
  }.observes('isShowing')

});
