/* globals $ */

import Ember from 'ember';

// Check usage examples in tc-components/inputs

export default Ember.Component.extend({
  type: 'text',

  ignoreValidIfNoMsg: true,

  classNameBindings: ['tcOk', 'tcWarning', 'tcError',
    'isInputFile:wrapper-input-file'],

  eventNames: ['change'],

  valid: '',

  inputFileElement() {
    return this.$('.input-file-trigger');
  },

  didInsertElement() {
    if (this.get('isInputFile')) {
      var elem = this.inputFileElement();
      this.eventNames
          .forEach(name => elem.on(name, {self:this}, this.handleSelectedFile));
    } else {
      this.$('.tc-input').on('focus', this.onFocusEvent);
    }
  },

  willDestroyElement() {
    if (this.get('isInputFile')) {
      var elem = this.inputFileElement();
      this.eventNames
        .forEach(name => elem.off(name, {self:this}, this.handleSelectedFile));
    } else {
      this.$('.tc-input').off('focus', this.onFocusEvent);
    }
  },

  onFocusEvent() {
    var $elem = $(this);
    $elem.one('click', function onClick(event) {
      var targetPosX = event.clientX -
                       $elem.offset().left -
                       parseInt($elem.css('padding-left')) -
                       parseInt($elem.css('border-left-width')) -
                       parseInt($elem.css('margin-left'));

      var clickAtTheEnd = (($elem.width() - targetPosX) < 10);
      if (clickAtTheEnd) {
        $elem.caretToEnd();
        $elem.scrollLeft(event.target.scrollWidth);
      }
    });
  },

  handleSelectedFile(event) {
    var files = event.target.files;
    var self = event.data.self || this;
    self.porcessingFiles(files, self);
  },

  handleEventFileDropZone: function(dropZone) {
    var files = dropZone.get('fileLoaded');
    this.porcessingFiles(files, this);
  }.observes('fileLoaded'),

  porcessingFiles(files, context) {
    if (!files || !files.length) {
      return;
    }

    if (!context.isFileAccepted(files)) {
      context.set('msg',
        Ember.I18n.t('components.tc-input.errorMessages.extension'));
      context.set('valid', 'error');
    } else {
      context.set('msg', '');
      context.set('valid', '');
    }

    var value = files && files[0] && files[0].name;
    context.set('value', value);
    context.set('files', files);
  },

  isFileAccepted(files) {
    if (!files){
      return false;
    }

    var isAccepted = true;
    if (!this.get('accept')){
      return isAccepted;
    }

    var acceptedValues = this.get('accept').split(',')
      .map(Function.prototype.call, String.prototype.trim).join('|');
    isAccepted = files[0].name
      .match(new RegExp('\.(' + acceptedValues + ')$'));

    return isAccepted;
  },

  hasMsg: function() {
    return this.get('valid') && !Ember.isBlank(this.get('msg'));
  }.property('valid', 'msg'),

  isInputFile: function() {
    return this.get('type') === 'file';
  }.property('type'),

  tcOk: function() {
    if (!this.get('msg') && this.get('ignoreValidIfNoMsg')) {
      return false;
    }
    return this.get('valid') === 'ok';
  }.property('valid', 'msg'),

  tcWarning: function() {
    if (!this.get('msg') && this.get('ignoreValidIfNoMsg')) {
      return false;
    }
    return this.get('valid') === 'warn';
  }.property('valid', 'msg'),

  tcError: function() {
    if (Ember.isBlank(this.get('msg')) && this.get('ignoreValidIfNoMsg')) {
     return false;
    }
    return this.get('valid') === 'error';
  }.property('valid', 'msg'),

  actions: {
    action() {
      this.sendAction();
    }
  }
});
