import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['wrapper-carousel'],
  caroselImagesPath: '/assets/images/carousel/',
  carouselImages: '',
  interval: 2000,
  carouselId: undefined,
  currentImageIndex: 0,
  intervalId: undefined,
  didInsertElement: function() {
    var self = this;
    var carouselImages = self.get('carouselImages').w();
    if (!Ember.isEmpty(self.get('carouselImages'))) {
      var imagePath = self.get('caroselImagesPath') + carouselImages[0];
      var currentImage = self.$('<img src="' + imagePath +
        '" class="carousel-current-image"/>');
      self.$("#" + self.get('carouselId')).append(currentImage);
    }

    var intervalId = window.setInterval (function() {
      self.showNext();
    }, self.get('interval'));
    self.set('intervalId', intervalId);
  },

  willDestroyElement: function() {
    if (!Ember.isEmpty(this.get('intervalId'))) {
      window.clearInterval(this.get('intervalId'));
    }
  },

  showNext: function() {
    var carouselImages = this.get('carouselImages').w();
    var currentImageIndex = this.get('currentImageIndex');
    var nextImageIndex = (currentImageIndex + 1) % carouselImages.length;
    this.set('currentImageIndex', nextImageIndex);
    var imagePath = this.get('caroselImagesPath');
    var nextImageSrc = imagePath + carouselImages[nextImageIndex];
    var carouselCurrentImage = this.$('.carousel-current-image');
    carouselCurrentImage
      .fadeOut(200, function() {
        carouselCurrentImage.attr("src", nextImageSrc);
      })
      .fadeIn(200);
  }
});
