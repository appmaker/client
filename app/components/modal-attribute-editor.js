import Ember from 'ember';

/*
 Usage:

 autorestoreAttrs is used to not save the changes of this key if the user
 cancel or close the modal

 {{#modal-attribute-editor [close="closeModal"] [save="action"]
    [cancel="action"] [model="model"] [autorestoreAttrs="key key2 key3"]}}
   {{yield}}
 {{/modal-attribute-editorl}}
*/

export default Ember.Component.extend({
  didInsertElement() {
    var model = this.get('model');
    //TODO issue #69 split in two components: tc-modal and attribute-editor
    this.set('_autorestore', this.get('autorestoreAttrs').w()
      .reduce((map, key) => {
        map[key] = model.get(key);
        return map;
      }, {}));

    Ember.run.next(this, 'addModalShow');
  },
  addModalShow() {
    this.sendAction('setup');
    this.$('.tc-modal').addClass('show');
  },
  removeModalShow(callback) {
    if (callback){
      this.$('.tc-modal').one(
        'webkitTransitionEnd otransitionend' +
        'oTransitionEnd msTransitionEnd transitionend',
        callback
      );
    }
    this.$('.tc-modal').removeClass('show');
  },
  cancelChanges() {
    Ember.run.next(this, () => {
      var model = this.get('model');
      var autorestore = this.get('_autorestore');
      Object.keys(autorestore).forEach(key =>
        model.set(key, autorestore[key]));
    });
  },
  actions: {
    close() {
      this.removeModalShow(() => this.sendAction('close'));
      this.cancelChanges();
    },
    cancel() {
      this.removeModalShow(() => this.sendAction('cancel'));
      this.cancelChanges();
    },
    save() {
      this.sendAction('save', this.get('model'));
    }
  }
});
