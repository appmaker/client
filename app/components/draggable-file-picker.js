import Ember from 'ember';

/*
  Usage:

  {{#draggable-file-picker [preview=true] [accept="extensions separate by coma"]
    [dropZone=true] [imgPreviewSrc=var] [placeholder=var]}}
  {{/draggable-file-picker}}
 */

export default Ember.Component.extend({
  classNames: ['draggable-file-picker'],
  classNameBindings: [
    'preview:show-preview'
  ],
  attributeBindings: ['disabled'],
  preview: false,
  dropZone: false,
  imgPreviewSrc: 'assets/images/default_img.png',
  disabled: false,
  isOverDragZone: false,
  placeholder: Ember.I18n.t('components.draggable-file-picker.placeholder'),
  valid: '',

  dropZoneElement() {
    return this.$('.wrapper-drop-zone');
  },

  didInsertElement() {
    if (this.get('dropZoneVisibility')) {
      var elem = this.dropZoneElement();
      elem.on('drop', {self:this}, this.handleDrop);
      elem.on('dragover', {self:this}, this.handlerDragOver);
      elem.on('dragleave', {self:this}, this.handlerDragLeave);
    }

    if (this.get('preview') && !this.get('imgPreviewSrc')) {
      this.set('imgPreviewSrc', 'assets/images/default_img.png');
    }
  },

  willDestroyElement() {
    if (this.get('dropZoneVisibility')) {
      var elem = this.dropZoneElement();
      elem.off('drop', {self:this}, this.handleDrop);
      elem.off('dragover', {self:this}, this.handlerDragOver);
      elem.off('dragleave', {self:this}, this.handlerDragLeave);
    }
  },

  dropZoneVisibility: function() {
    return this.get('dropZone') && !this.get('disabled');
  }.property('dropZone', 'disabled'),

  previewBackground: function() {
    if (this.get('valid') === 'error') {
      return ('background-image: none').htmlSafe();
    }
    var url = this.get('imgPreviewSrc');
    return ('background-image:url(\'' + url + '\')').htmlSafe();
  }.property('valid', 'imgPreviewSrc'),

  handleDrop: function (event) {
    if (event.type !== 'drop') {
      return;
    }

    if (event.preventDefault) {
      event.preventDefault();
    }

    var self = event.data.self;
    var files = event.dataTransfer.files;
    self.set('isOverDragZone', false);
    self.set('fileLoaded', files);
    self.handleFiles(files);
  }.observes('files'),

  handleSelectedFile: function () {
    var files = this.get('files');
    this.handleFiles(files);
  }.observes('files'),

  handleFiles(files) {
    this.updatePreview(files);
    this.sendAction('fileChanged', files[0]);
  },

  handlerDragLeave: function (event) {
    var self = event.data.self || this;
    self.set('isOverDragZone', false);
  },

  handlerDragOver: function (event) {
    var self = event.data.self || this;
    if(!self.get('isOverDragZone')) {
      self.set('isOverDragZone', true);
    }
  },

  updatePreview(files) {
    if (!this.get('preview') || !files) {
      return;
    }
    this.readFile(files[0], 'readAsDataURL').then((imgFile) => {
      this.set('imgPreviewSrc', imgFile.data);
    });
  },

  readFile(file, readAs) {
    var reader = new FileReader();

    return new Ember.RSVP.Promise(function(resolve, reject) {
      reader.onload = function(event) {
        resolve({
          filename: file.name,
          type: file.type,
          data: event.target.result,
          size: file.size
        });
      };

      reader.onabort = function() {
        reject({ event: 'onabort' });
      };

      reader.onerror = function(error) {
        reject({ event: 'onerror', error: error });
      };

      reader[readAs](file);
    }.bind(this));
  },

  hasMsg: function() {
    return this.get('valid') && this.get('msg');
  }.property('valid', 'msg')
});
