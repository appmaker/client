import Ember from 'ember';
import config from '../config/environment';

export default Ember.Component.extend({
  actions: {
  },
  jobCompleted: function(result) {
    this.sendAction('action', result);
    this.set('isLoading', false);
  },
  isLoadingObserver: function() {
    var self = this;
    if (this.get('jobId')) {
      this.set('isLoading', true);
      var delay = this.get('delay') || 500;
      Ember.run.later(function polling() {
        this.$.get(config.apiHost + '/appmaker/v1/works/' + self.get('jobId'),
          function(data) {
            if (data.status === 0) {
              Ember.run.later(polling, delay);
            } else {
              self.jobCompleted(data);
            }
          }
        );
      }, delay);
    }
  }.observes('jobId')
});
