import Ember from 'ember';

/*
 Description: It is used to change the label of a component according
 to a condition

 Usage:

 {{conditional-label label=string alternativeLabel=string
  alternativeIcon="fa-icon name" condition=true|false}}
*/

export default Ember.Component.extend({
  tagName: 'span',
  classNameBindings: ['condition:alternative-label-displayed']
});
