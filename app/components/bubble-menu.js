import Ember from 'ember';

export default Ember.Component.extend({

  onShowMenu: function() {
    var method = this.get('showMenu') ? 'on' : 'off';
    this.$(window)[method]('click', this.hideMenuBound);
  }.observes('showMenu'),

  didInsertElement() {
    this.set('hideMenuBound', this.hideMenu.bind(this));
    this.set('toggleButton', this.get('element').
                                  querySelector('.toggle-menu-icon'));
  },

  hideMenu(event) {
    if (event.target === this.get('toggleButton')) {
      // This early return avoid hiding the menu immediately once users click on
      // toggle button because this sets the variable 'showMenu' to true then
      // 'onShowMenu' observer is executed and the 'click' handler on window is
      // dispatched hiding the menu again. Of course, we can prevent bubbling
      // on the component template but menus wouldn't be closed clicking others.
      return;
    }
    this.set('showMenu', false);
  },

  actions: {
    toggleMenu() {
      this.toggleProperty('showMenu');
    },

    edit() {
      var conf = {
        template: 'modal-edit-project-name',
        model: this.get('model')
      };
      this.sendAction('edit', conf);
    },

    delete() {
      this.sendAction('delete');
    }
  }
});
