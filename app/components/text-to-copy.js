import Ember from 'ember';

/*
 Usage:

 {{#text-to-copy}}
   The place to put the text to select.
   WARNING, if you write break lines here, they will create spaces and then
   they are selected and copy. If you don't want copy inconsistent spaces be
   carefull with it.
 {{/text-to-copy}}
*/

export default Ember.Component.extend({
  showToast: false,

  showMessage: false,

  actions: {
    selectText: function(){
      var text = this.$('.text-to-copy')[0];
      var range = null;
      var selection = null;

      if (document.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(text);
        range.select();
      } else if (window.getSelection) {
        selection = window.getSelection();
        range = document.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
      }

      this.set('showMessage', true);
    }
  }
});
