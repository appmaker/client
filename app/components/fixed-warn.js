import Ember from 'ember';

/*
 Usage:

 {{#fixed-warn id="string"}}
  Yield
 {{/fixed-warn}}
*/

export default Ember.Component.extend({
  tagName: 'div',
  classNames: ['fixed-warn'],
  attributeBindings: ['id'],
  id: ''
});
