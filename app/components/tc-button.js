import Ember from 'ember';

// Check usage examples in tc-components/inputs

export default Ember.Component.extend({
  tagName: 'span',
  classNameBindings: [ 'block:tc-row'],
  click: function() {
    this.sendAction();
  }
});
