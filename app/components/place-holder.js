import Ember from 'ember';

/*
 Usage:

 {{place-holder default=string value=string}}
*/

export default Ember.Component.extend({
  tagName: 'span',
  classNameBindings: ['placeholder'],
  textChanges: function() {
    if (!this.get('value')) {
      this.set('placeholder', true);
    } else {
      this.set('placeholder', false);
    }
  }.observes('value').on('init')
});
