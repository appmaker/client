import Ember from 'ember';

export default function readFile(file, readAs) {
  var reader = new FileReader();

  return new Ember.RSVP.Promise(function(resolve, reject) {
    reader.onload = function(event) {
      resolve({
        filename: file.name,
        type: file.type,
        data: event.target.result,
        size: file.size
      });
    };

    reader.onabort = function() {
      reject({ event: 'onabort' });
    };

    reader.onerror = function(error) {
      reject({ event: 'onerror', error: error });
    };

    reader[readAs](file);
  }.bind(this));
}
