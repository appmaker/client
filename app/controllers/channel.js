import Ember from 'ember';
import config from '../config/environment';
import EmberValidations from 'ember-validations';
import ChannelBase from './channel-base';
import Statistics from '../statistics/statistics';

var ChannelController = ChannelBase.extend(EmberValidations.Mixin, {
  needs:['project'],

  deleteMode: false,

  appHost: config.deploymentHost,

  showErrors: false,

  srcAttachment: null,

  loading: false,

  channelSaved: false,

  isZipFileOrigin: function() {
    var type = this.get('content.origin.type.id');
    return (type === 'zip');
  }.property('content.origin.type'),

  isGitOrigin: function() {
    var type = this.get('content.origin.type.id');
    return (type === 'gh');
  }.property('content.origin.type'),

  isSSHOrigin: function() {
    var type = this.get('content.origin.type.id');
    return (type === 'ssh');
  }.property('content.origin.type'),

  isAppmakerDeployment: function() {
    var type = this.get('content.deployment.type.id');
    return (type === 'appmaker');
  }.property('content.deployment.type'),

  isGithubDeployment: function() {
    var type = this.get('content.deployment.type.id');
    return (type === 'github');
  }.property('content.deployment.type'),

  isSSHDeployment: function() {
    var type = this.get('content.deployment.type.id');
    return (type === 'ssh');
  }.property('content.deployment.type'),

  validations: {
    'appInfo.name': {
      presence: {
        'if': function(channel) {
          return Ember.isBlank(channel.get('model.appInfo.name'));
        }
      }
    },

    'appInfo.description': {
      presence: {
        'if': function(channel) {
          return Ember.isBlank(channel.get('model.appInfo.description'));
        }
      }
    },

    'appInfo.iconUrl': {
      presence: {
        'if': function(channel) {
          return Ember.isEmpty(channel.get('model.appInfo.iconUrl'));
        }
      }
    },

    deployment: {
      presence: {
        'if': function(channel) {
          var deploymentType = channel.get('model.deployment.type.id');
          var deploymentParams = channel.get('model.deployment.params');
          var result = true;
          switch (deploymentType) {
            case 'appmaker':
              result = Ember.isBlank(deploymentParams.subdomain);
              break;
            case 'github':
              result = Ember.isBlank(deploymentParams.branch);
              break;
            case 'ssh':
              var ssh = deploymentParams.ssh;
              result = (!ssh) || (!ssh.file) || (!ssh.url) ||
                (Ember.isBlank(ssh.file.header) &&
                Ember.isBlank(ssh.url.header));
              break;
            default:
              result = true;
          }
          return result;
        }
      },
      format: {
        /* TODO Issue #79
          See bug https://github.com/dockyard/ember-validations/issues/169
          (2.0-blocker)
        message: Ember.I18n.t('errors.format'),
        now, the message showed is 'isInvalid'
        */
        if: function(channel) {
          var deploymentType = channel.get('model.deployment.type.id');
          var deploymentParams = channel.get('model.deployment.params');
          var subdomainPattern = '^[A-Za-z0-9]+([A-Za-z0-9-_]*[A-Za-z0-9]+)?$';
          var result = true;
          switch (deploymentType) {
            case 'appmaker':
              var subDomainRegex = new RegExp(subdomainPattern);
              result = !subDomainRegex.test(deploymentParams.subdomain);
              break;
            case 'github':
              var ghBranchRegex = new RegExp(subdomainPattern);
              result = !ghBranchRegex.test(deploymentParams.subdomain);
              break;
            case 'ssh':
              //TODO: when the service is implemented on the server side
              result = false;
              break;
            default:
              result = true;
          }
          return result;
        }
      }
    }
  },

  isInvalidSubdomain: function() {
    var result = false;
    var errors = this.get('errors');
    var deploymentType = this.get('model.deployment.type.id');
    var subdomain = this.get('model.deployment.params.subdomain');
    if (subdomain) {
      subdomain = subdomain.toLowerCase();
      this.set('model.deployment.params.subdomain', subdomain);
      if (deploymentType === 'appmaker' && subdomain) {
        var storedProjects = this.store.all('project');
        var channelId = this.get('model.id');
        var matchingSubdomain = storedProjects.find(prj =>
          (((channelId !== prj.get('beta.id')) &&
          prj.get('beta.deployment.params.subdomain') === subdomain) ||
          ((channelId !== prj.get('production.id')) &&
          prj.get('production.deployment.params.subdomain') ===
          subdomain)));
        if (!Ember.isEmpty(matchingSubdomain)) {
          this.addSubdomainError(errors);
          result = true;
        }
      }
      this.set('errors', errors);
    }
    return result;
  }.property('content.deployment.params.subdomain', 'showErrors', 'errors'),

  hasErrors: function() {
    return this.get('isInvalidSubdomain') ||
           this.get('isTooLongSubdomain') ||
           this.get('iconValid') === 'error' ||
           Ember.isBlank(this.get('model.appInfo.name')) ||
           Ember.isBlank(this.get('model.appInfo.description'));
  }.property('isInvalidSubdomain', 'isTooLongSubdomain', 'iconValid',
             'model.appInfo.name', 'model.appInfo.description'),

  sendStatistic(StatisticClass) {
    var originType = 'model.origin.type.id';
    var params = {
      tag: this.get('model.tag'),
      origin: this.get(originType)
    };

    if (Ember.isNone(params.origin)) {
      this.addObserver(originType, this, function observe() {
        this.removeObserver(originType, this, observe);
        params.origin = this.get(originType);
        this.send('statistics', new StatisticClass(params));
      });
    } else {
      this.send('statistics', new StatisticClass(params));
    }
  },

  initModule() {
    this.set('srcAttachment', null);
    this.set('fileValue', '');
    this.set('valid', '');
    this.set('msg', '');
    // Ember validations will automatically run when the object is created and
    // when each property changes. But this is not working properly when
    // properties are objects. Initially appInfo.name and appInfo.description
    // are empty (invalid -> string errors are added). We have to force
    // the validation process once we know the record is already ready just
    // in case users type a subdomain already in use and we would show this
    // error but also the name and description presence initially not defined.
    this.validate();
    this.sendStatistic(Statistics.Channel.Updating);
  },

  _saveChannel() {
    this.validate().then(() => {
      if (this.get('isInvalidSubdomain') ||
          this.get('isTooLongSubdomain')) {
        this.set('showErrors', true);
      } else {
        this.set('showErrors', false);
        this.set('loading', true);
        this.get('model').save()
            .then(() => {
              this.sendStatistic(Statistics.Channel.Updated);
              this.set('loading', false);
              this.set('channelSaved', true);
            })
            .catch(reason => {
              this.onError(reason);
              this.set('loading', false);
            });
      }
    }, () => {
      this.set('showErrors', true);
      this.set('loading', false);
    });
  },

  actions: {
    goBack() {
      var project = this.get('model');
      if (project) {
        project.rollback();
      }
      this.transitionToRoute('projects');
    },

    delete() {
      this.set('deleteMode', true);
    },

    cancelDelete() {
      this.set('deleteMode', false);
    },

    confirmDelete() {
      this.sendStatistic(Statistics.Channel.Deleted);
      // this will tell Ember-Data to delete the current record
      this.get('model').destroyRecord();
      this.transitionToRoute('projects');
      this.set('deleteMode', false);
    },

    fileLoaded(fileUploaded) {
      this.set('srcAttachment', fileUploaded);
    },

    save() {
      var file = this.get('srcAttachment');
      if (file) {
        var type = this.get('model.origin.type.id');
        if (type === 'zip') {
          var newOrigin = this.store.createRecord('origin');
          this.store.find('originType', type).then((originType) => {
            newOrigin.set('type', originType);
            newOrigin.set('src', file);
            newOrigin.save()
              .then((originSaved) => {
                this.set('model.origin', originSaved);
                this._saveChannel();
              })
              .catch(error =>
                {
                  newOrigin.deleteRecord();
                  console.error('Error saving origin', error);
                });
          });
        } else {
          this._saveChannel();
        }
      } else {
        this._saveChannel();
      }
    },

    setup() {
      this.initModule();
      this.initDefaultOriginType();
    }
  }
});

export default ChannelController;
