import Ember from 'ember';

export default Ember.Controller.extend({
  drawLightHeader: false,

  updateDrawLightHeader: function() {
    var drawLightHeader = false;
    if (this.parentController.currentPath === 'about') {
      drawLightHeader = true;
    }
    this.set('drawLightHeader', drawLightHeader);
  }.observes('this.parentController.currentPath'),

  actions: {
    invalidateSession() {
      this.get('session').invalidate();
    }
  }
});
