import Ember from 'ember';
import ErrorMixin from '../mixins/error-mixin';
import Statistics from '../statistics/statistics';

export default Ember.Controller.extend(ErrorMixin, {

  defaultOriginType: 'zip',

  maxSubdomainLength: 63,

  initDefaultOriginType() {
    this.set('defaultOriginType', 'zip');
  },

  handleServerError(error) {
    if (error.message === 'subdomain-already-exists') {
      this.onSubdomainAlreadyExists();
      this.send('statistics', new Statistics.Notification.Fail.Conflict({
        name: 'Subdomain'
      }));
    } else {
      this._super(error);
    }
  },

  onSubdomainAlreadyExists() {
    var errors = this.get('errors');
    this.addSubdomainError(errors);
    this.set('errors', errors);
    this.set('showErrors', true);
  },

  addSubdomainError(errors) {
    var label = Ember.I18n.t('channel.deployment.appmaker.description');
    var text = Ember.I18n.t('errors.uniqueness', {field: label});
    errors.deployment.clear();
    errors.deployment.pushObject(text);
  },

  isTooLongSubdomain: function() {
    var isTooLong = false;
    var deploymentSubdomain = this.get('model.deployment.params.subdomain');
    if (deploymentSubdomain) {
      var deploymentType = this.get('model.deployment.type.id');
      if (!deploymentType || deploymentType === 'appmaker') {
        var maximum = this.get('maxSubdomainLength');
        isTooLong = (deploymentSubdomain.length > maximum);
        if (isTooLong) {
          var text = Ember.I18n.t('errors.tooLong', {count: maximum});
          var errors = this.get('errors');
          errors.deployment.clear();
          errors.deployment.pushObject(text);
          this.set('errors', errors);
        }
      }
    }
    return isTooLong;
  }.property('content.deployment.params.subdomain'),

  watchSubdomain: function() {
    if (this.get('loadingDeploy')) {
      return;
    }

    this.set('showErrors', this.get('isInvalidSubdomain') ||
                           this.get('isTooLongSubdomain'));
  }.observes('model.deployment.params.subdomain'),

  actions: {
    iconLoaded(iconUploaded) {
      var appInfo = this.get('model.appInfo');
      // TODO: https://gitlab.com/appmaker/server/issues/12
      // Please uncomment next line and delete the next one once the issue
      // will be resolved on server side.
      // appInfo.set('icon', iconUploaded);
      appInfo.set('iconUrl', iconUploaded.data);
    }
  }

});
