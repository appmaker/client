import Ember from 'ember';
import EmberValidations from 'ember-validations';
import config from '../../config/environment';
import ChannelBase from '../channel-base';
import Statistics from '../../statistics/statistics';

var ChannelController = ChannelBase.extend(EmberValidations.Mixin,
                                           Ember.Evented, {

  queryParams: ['name', 'tag'],

  name: null,

  tag: null,

  needs: ['project'],

  project: Ember.computed.alias("controllers.project"),

  step: 1,

  savedChannel: false,

  appHost: config.deploymentHost,

  srcAttachment: null,

  loadingOrigin: false,

  loadingDeploy: false,

  validations: {
    'appInfo.name': {
      presence: {
        'if': function(channel) {
          return Ember.isBlank(channel.get('model.appInfo.name'));
        }
      }
    },

    'appInfo.description': {
      presence: {
        'if': function(channel) {
          return Ember.isBlank(channel.get('model.appInfo.description'));
        }
      }
    },

    'appInfo.iconUrl': {
      presence: {
        'if': function(channel) {
          return Ember.isEmpty(channel.get('model.appInfo.iconUrl'));
        }
      }
    },

    deployment: {
      presence: {
        'if': function(channel) {
          var deploymentType = channel.get('model.deployment.type.id');
          var deploymentParams = channel.get('model.deployment.params');
          var result= true;
          switch (deploymentType) {
            case 'appmaker':
              result = Ember.isBlank(deploymentParams.subdomain);
              break;
            case 'github':
              result = Ember.isBlank(deploymentParams.branch);
              break;
            case 'ssh':
              var ssh = deploymentParams.ssh;
              result = (!ssh) || (!ssh.file) || (!ssh.url) ||
                (Ember.isBlank(ssh.file.header) &&
                Ember.isBlank(ssh.url.header));
              break;
            default:
              result = true;
          }
          return result;
        }
      },
      format: {
        /* TODO Issue #79
          See bug https://github.com/dockyard/ember-validations/issues/169
          (2.0-blocker)
        message: Ember.I18n.t('errors.format'),
        now, the message showed is 'isInvalid'
        */
        if: function(channel) {
          var deploymentType = channel.get('model.deployment.type.id');
          var deploymentParams = channel.get('model.deployment.params');
          var subdomainPattern = '^[A-Za-z0-9]+([A-Za-z0-9-_]*[A-Za-z0-9]+)?$';
          var result = true;
          switch (deploymentType) {
            case 'appmaker':
              var subDomainRegex = new RegExp(subdomainPattern);
              result = !subDomainRegex.test(deploymentParams.subdomain);
            break;
            case 'github':
              var ghBranchRegex = new RegExp(subdomainPattern);
              result = !ghBranchRegex.test(deploymentParams.subdomain);
              break;
            case 'ssh':
              //TODO: when the service is implemented on the server side
              result = false;
              break;
            default:
              result = true;
          }
          return result;
        }
      }
    },
    name: { presence: true },
    tag: { presence: true }
  },

  showErrors: false,

  isInvalidSubdomain: function() {
    var result = false;
    var errors = this.get('errors');
    var deploymentType = this.get('model.deployment.type.id');
    var subdomain = this.get('model.deployment.params.subdomain');
    if (subdomain) {
      subdomain = subdomain.toLowerCase();
      this.set('model.deployment.params.subdomain', subdomain);
      if (deploymentType === 'appmaker' && subdomain) {
        var storedProjects = this.store.all('project');
        var matchingSubdomain = storedProjects.find(prj =>
          ((prj.get('beta.id') &&
          prj.get('beta.deployment.params.subdomain') === subdomain) ||
          (prj.get('production.id') &&
          prj.get('production.deployment.params.subdomain') ===
          subdomain)));
        if (!Ember.isEmpty(matchingSubdomain)) {
          this.addSubdomainError(errors);
          result = true;
        }
      }
      this.set('errors', errors);
    }
    return result;
  }.property('content.deployment.params.subdomain', 'showErrors', 'errors'),


  disableSaveOriginButton: function() {
    var valid = this.get('valid');
    var step = this.get('step');
    var disableButton = false;
    if (step > 1 || valid === 'error') {
      disableButton = true;
    } else {
      var originType = this.get('model.origin.type.id');
      var originParams = this.get('model.origin.params');

      switch (originType) {
        case 'zip':
          if (!this.get('srcAttachment')) {
            disableButton = true;
          }
          break;
        case 'gh':
          disableButton = Ember.isBlank(originParams.repository);
          break;
        case 'ssh':
          disableButton = Ember.isBlank(originParams.fileName);
          break;
        default:
          disableButton = true;
          break;
      }
    }
    return disableButton;
  }.property('step', 'model.origin.type', 'model.origin.params',
             'srcAttachment', 'valid'),

  disableSaveManifest: function() {
    return this.get('showStep3') || this.get('iconValid') === 'error' ||
           Ember.isBlank(this.get('model.appInfo.name')) ||
           Ember.isBlank(this.get('model.appInfo.description'));
  }.property('showStep3', 'iconValid', 'model.appInfo.name',
             'model.appInfo.description'),

  disableSaveDeployment: function() {
    return this.get('isInvalidSubdomain') ||
           this.get('isTooLongSubdomain');
  }.property('isInvalidSubdomain', 'isSubdomainTooLong'),

  showStep1: function() {
    var step = this.get('step');
    return (step >= 1);
  }.property('step'),

  showStep2: function() {
    var step = this.get('step');
    return (step >= 2);
  }.property('step'),

  showStep3: function() {
    var step = this.get('step');
    var isStep3 = (step === 3);
    if (isStep3) {
      this.set('showErrors', this.get('isInvalidSubdomain') ||
                             this.get('isTooLongSubdomain'));
    }
    return isStep3;
  }.property('step'),

  isZipFileOrigin: function() {
    var type = this.get('model.origin.type.id');
    return (type === 'zip');
  }.property('model.origin.type'),

  isGitOrigin: function() {
    var type = this.get('model.origin.type.id');
    return (type === 'gh');
  }.property('model.origin.type'),

  isSSHOrigin: function() {
    var type = this.get('model.origin.type.id');
    return (type === 'ssh');
  }.property('model.origin.type'),

  isAppmakerDeployment: function() {
    var type = this.get('model.deployment.type.id');
    return (type === 'appmaker');
  }.property('model.deployment.type'),

  isGithubDeployment: function() {
    var type = this.get('model.deployment.type.id');
    return (type === 'github');
  }.property('model.deployment.type'),

  isSSHDeployment: function() {
    var type = this.get('model.deployment.type.id');
    return (type === 'ssh');
  }.property('model.deployment.type'),

  toSubdomain(str) {
    str = str.trim().replace(/\s+/g,'-').toLowerCase();

    // XXX: map with substitutions. regexp: value
    // TODO: Maybe we should replace with an array to ensure the order...
    var dict = {
      'á':'a',
      'é':'e',
      'í':'i',
      'ó':'o',
      '[úü]':'u',
      '[^\\w]': ''
    };

    Object.keys(dict).forEach(chars => str = str.replace(new RegExp(chars, 'g'),
                                                         dict[chars]));
    return str;
  },

  initSubdomain() {
    var subdomain = this.toSubdomain(this.get('project.model.name'));

    var tag = this.get('tag');
    if (tag !== 'production') {
      subdomain = subdomain + '-' + tag;
    }

    this.set('model.deployment.params.subdomain', subdomain.toLowerCase());
  },

  initName() {
    var name = Ember.I18n.t('channel.manifest.name.' + this.get('tag'), {
      name: this.get('project.model.name')
    });
    this.set('model.appInfo.name', name);
  },

  initDescription() {
    var desc = Ember.I18n.t('channel.manifest.description.' + this.get('tag'), {
      name: this.get('project.model.name')
    });
    this.set('model.appInfo.description', desc);
  },

  initModule() {
    this.set('step', 1);
    this.set('savedChannel', false);
    this.set('srcAttachment', null);
    this.set('loadingOrigin', false);
    this.set('loadingDeploy', false);
    this.set('showErrors', false);
    this.send('statistics', new Statistics.Channel.Creating({
      tag: this.get('tag')
    }));
  },

  initFields() {
    this.initName();
    this.initDescription();
    this.initSubdomain();
  },

  initPreview() {
    var src = 'assets/images/default_' + this.get('tag') + '.png';
    this.set('model.appInfo.iconUrl', src);
  },

  initOrigin() {
    this.set('fileValue', '');
    this.set('valid', '');
    this.set('msg', '');
    this.trigger('resetOrigin');
  },

  actions: {
    goBack: function() {
      var project = this.get('model');
      if (project) {
        project.deleteRecord();
      }
      this.set('showErrors', false);
      this.transitionToRoute('projects');
    },

    fileLoaded: function(fileUploaded) {
      this.set('srcAttachment', fileUploaded);
    },

    saveOrigin: function() {
      var origin = this.get('model.origin');
      var type = this.get('model.origin.type.id');
      var nextStep = false;

      // TODO: What about generalizing the way we work with form validation or
      // look for an ember addon to do it?
      var file = this.get('srcAttachment');

      if (type === 'zip' && file) {
        origin.set('src', file);
        nextStep = true;
      }

      if (type === 'gh' && origin.get('params').get('repository')) {
        nextStep = true;
      }

      // TODO: Add warning for incorrect/mandatory fields
      if (nextStep) {
        this.set('loadingOrigin', true);
        origin
          // XXX: origin is an async relationship. So, and this is missed from
          // the documentation, origin isn't an EmberObject but a PromiseObject.
          // You can use it as an object because it is proxying data but you can
          // not save() without accessing the contents, who the hell knows why.
          .then(content => content.save())
          .then(() => {
            this.set('step', 2);
            this.set('loadingOrigin', false);
            Ember.run.next(this, () => this.trigger('originEnd'));
          })
          .catch(error => {
            this.set('loadingOrigin', false);
            console.log('Error', error);
          });
      } else {
        console.log('No esta cargado el fichero, aviso o no hago nada');
      }
    },

    saveManifest: function() {
      this.validate().then(() => {
        this.set('showErrors', false);
        this.set('step', 3);
      },
        () => {
          var errors = this.get('errors');

          var noErrors = Object.keys(errors.appInfo)
            .every(field => Ember.isBlank(errors.appInfo[field]));

          if (noErrors) {
            this.set('showErrors', false);
            this.set('step', 3);
            Ember.run.next(this, () => this.trigger('manifestEnd'));
          } else {
            this.set('showErrors', true);
          }
      });
    },

    save: function() {
      this.validate().then(() => {
        if (this.get('isValid')) {
          if (this.get('isInvalidSubdomain') ||
              this.get('isTooLongSubdomain')) {
            this.set('showErrors', true);
          } else {
            this.set('showErrors', false);
            this.set('loadingDeploy', true);
            this.get('model').save()
              .then(() => {
                this.set('savedChannel', true);
                this.send('statistics', new Statistics.Channel.Created({
                  tag: this.get('tag'),
                  origin: this.get('model.origin.type.id')
                }));
                var conf = {
                  template: 'modal',
                  view: 'channels/modal-success'
                };
                this.send('openModal', conf);
              }, this.onError.bind(this))
              .then(() => {
                this.set('loadingDeploy', false);
              })
              .catch(error => {
                this.set('loadingDeploy', false);
                console.error('Error', error);
              });
          }
        }
      }, () => {
        this.set('showErrors', true);
      });
    },

    setup() {
      this.initModule();
      this.initFields();
      this.initPreview();
      this.initDefaultOriginType();
      this.initOrigin();
    }
  }
});

export default ChannelController;
