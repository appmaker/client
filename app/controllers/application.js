/* globals StatisticsManager, _urq */

import Ember from 'ember';
import ErrorMixin from '../mixins/error-mixin';

export default Ember.Controller.extend(ErrorMixin, Ember.Evented, {

  queryParams: ['feedback', 'statistics'],

  feedback: null,

  statistics: null,

  feedbackDialogAlreadyDisplayed: true,

  showCookiesWarning: true,

  init() {
    this._super.apply(this, arguments);

    var cookie = this.get('cookie');

    if (!cookie.getCookie('feedbackDialogAlreadyDisplayed')) {
      this.set('feedbackDialogAlreadyDisplayed', false);
    }

    if (!cookie.getCookie('showCookiesWarning')) {
      this.set('showCookiesWarning', false);
    }

    Ember.RSVP.on('error', e => {
      this.onError(e);
    });
  },

  watchFeedback: function() {
    if (Ember.isNone(this.get('feedback'))) {
      return;
    }
    this.trigger('onFeedbackChanged');
  }.observes('feedback'),

  watchStatistics: function() {
    if (Ember.isNone(this.get('statistics'))) {
      return;
    }
    var enable = this.get('statistics') === 'false' ? false : true;
    StatisticsManager.setState(enable);
  }.observes('statistics'),

  actions: {
    minimizeFeedbackDialog() {
      var cookie = this.get('cookie');
      cookie.setCookie('feedbackDialogAlreadyDisplayed', true).then(() => {
        this.set('feedbackDialogAlreadyDisplayed', true);
      });
    },

    openFeedbackIframe() {
      if (!this.get('feedbackDialogAlreadyDisplayed')) {
        this.trigger('onFeedbackMinimize', () => {
          Ember.run.later(() => _urq.push(['Feedback_Open']), 100);
        });
        this.send('minimizeFeedbackDialog');
      } else {
        _urq.push(['Feedback_Open']);
      }
    },

    acceptCookies() {
      this.trigger('onAcceptCookies', () => {
        var cookie = this.get('cookie');
        cookie.setCookie('showCookiesWarning', true).then(() => {
          this.set('showCookiesWarning', true);
        });
      });
    }
  }
});
