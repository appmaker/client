import Ember from 'ember';
import CurrentControllerMixin from '../mixins/current-controller';
import Statistics from '../statistics/statistics';

export default Ember.Controller.extend(CurrentControllerMixin, {

  // XXX: needed by the mixin
  needs: ['application'],

  deleteMode: false,

  editMode: false,

  getTag() {
    var tagProp = 'currentController.model.tag';
    return this.get(tagProp);
  },

  isShowingBetaChannel: function() {
    return this.getTag() === 'beta';
  }.property('currentController.model.tag'),

  isShowingProductionChannel: function() {
    return this.getTag() === 'production';
  }.property('currentController.model.tag'),

  actions: {
    cancelDelete: function(){
      // set deleteMode back to false
      this.set('deleteMode', false);
    },

    confirmDelete: function(){
      // this will tell Ember-Data to delete the current record
      this.get('model').deleteRecord();
      this.get('model').save().then(() => {
        this.send('statistics', new Statistics.Project.Deleted());
      });
      // and then go to the users route
      this.transitionToRoute('projects');
      // set deleteMode back to false
      this.set('deleteMode', false);
    },

    delete: function(){
      this.set('deleteMode', true);
    },

    enableEdit : function() {
      this.set('editMode', true);
    },

    goBack: function() {
      var project = this.get('model');
      if (project) {
        project.rollback();
      }
      this.setProperties({
        'deleteMode': false,
        'editMode': false
      });
      this.transitionToRoute('projects');
    },
    save: function(appProject) {
      appProject.save();
      this.setProperties({
        'deleteMode': false,
        'editMode': false
      });
      this.transitionToRoute('projects');
    }
  }
});
