import Ember from 'ember';
import ProjectsBase from './projects-base';
import EmberValidations from 'ember-validations';
import ErrorMixin from '../mixins/error-mixin';
import Statistics from '../statistics/statistics';

export default Ember.Controller.extend(ErrorMixin,
                                       ProjectsBase,
                                       EmberValidations.Mixin,
                                       Ember.Evented, {
  getFieldLabel() {
    return Ember.I18n.t('project.edit.label');
  },

  actions: {
    save(appProject) {
      var projectName = this.sanitizedProjectName();
      if (projectName === this.get('currentName')) {
        this.send('closeModal');
        return;
      }

      appProject.set('name', projectName);
      appProject.save().then(() => {
        this.trigger('saved');
        this.send('statistics', new Statistics.Project.Updated());
      }).catch(this.onError.bind(this));
    },

    setup() {
      var currentName = this.get('model.name');
      this.set('currentName', currentName);
      this.set('projectName', currentName);
    }
  }
});
