import Ember from 'ember';
import Statistics from '../statistics/statistics';

export default Ember.Mixin.create({

  currentName: '',

  projectName: '',

  showErrors: false,

  validations: {
    projectName: {
      presence: true
    }
  },

  sanitizedProjectName() {
    return (this.get('projectName') || '').trim();
  },

  handleServerError(error) {
    if (error.message === 'Project already exists') {
      this.onProjectAlreadyExists();
      this.send('statistics', new Statistics.Notification.Fail.Conflict({
        name: 'Project name'
      }));
    } else {
      this._super(error);
    }
  },

  onProjectAlreadyExists() {
    var errors = this.get('errors');
    this.loadMessageError(errors);
    this.set('errors', errors);
    this.set('showErrors', true);
  },

  getFieldLabel() {
    return '';
  },

  loadMessageError(errors) {
    var projectFieldLabel = this.getFieldLabel();
    errors.set('projectName', Ember.I18n.t('errors.uniqueness',
                                           {field: projectFieldLabel}));
  },

  /**
   * Returns true when the new name is equal to some existing name and this is
   * not equal to original name.
   *
   * @param {String} Project name on store
   * @param {String} Project name what user is typing
   */
  nameExists(existingName, newName) {
    return (existingName === newName) &&
           (newName !== this.get('currentName'));
  },

  isInvalidProjectName: function() {
    var newProjectName = this.sanitizedProjectName();
    var storedProjects = this.store.all('project'); // local store
    var matchingProject = storedProjects
      .find(project => this.nameExists(project.get('name'), newProjectName));
    var defaultValidationsPassed = this.get('isValid');
    if (defaultValidationsPassed) {
      var errors = this.get('errors');
      if (!Ember.isEmpty(matchingProject)) {
        this.loadMessageError(errors);
        this.set('showErrors', true);
      } else {
        delete errors.projectName;
        this.set('showErrors', false);
      }
      this.set('errors', errors);
    }
    return (!defaultValidationsPassed || !Ember.isEmpty(matchingProject));
  }.property('projectName')

});
