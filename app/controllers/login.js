import Ember from 'ember';
import config from '../config/environment';

export default Ember.Controller.extend({
  getPathFromURL(url) {
    return url.split(/[?#]/)[0];
  },

  getGitHubEndpoint() {
    return config.apiHost +
           '/appmaker/v1/auth/github?redirectUrl=' +
           this.getPathFromURL(this.get('previousURL'));
  },

  actions: {
    authenticate() {
      window.location.replace(this.getGitHubEndpoint());
    }
  }
});
