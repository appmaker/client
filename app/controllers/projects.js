import Ember from 'ember';
import ProjectsBase from './projects-base';
import EmberValidations from 'ember-validations';
import ErrorMixin from '../mixins/error-mixin';
import Statistics from '../statistics/statistics';

var ProjectsController = Ember.ArrayController.extend(ErrorMixin,
                                                      ProjectsBase,
                                                      EmberValidations.Mixin,
                                                      Ember.Evented, {
  sortProperties: ['creationTime'],

  sortAscending: false,

  getFieldLabel() {
    return Ember.I18n.t('projects.new.name.label');
  },

  actions: {
    create: function() {
      var defaultValidationsPassed = this.get('isValid');
      if (defaultValidationsPassed && (!this.get('isInvalidProjectName'))) {
        this.set('showErrors', false);
        var newProjectName = this.sanitizedProjectName();
        var newProject = this.store.createRecord('project', {});
        newProject.set('name', newProjectName);
        newProject.set('creationTime', new Date());
        newProject.save()
          .then(() => {
            this.set('projectName', '');
            this.send('statistics', new Statistics.Project.Created());
          })
          .catch(reason => {
            Ember.run.later(() => newProject.destroyRecord(), 600);
            this.onError(reason);
          });
      }
    },

    newProjectFocusStatusHandler(){
      this.trigger('onfocus');
    }
  }
});

export default ProjectsController;
