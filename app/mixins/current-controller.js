import Ember from 'ember';

/**
 * This mixin exposes the current controller. You must add 'application' to
 * the controller's [`needs`]
 * (http://emberjs.com/api/classes/Ember.Controller.html#property_needs)
 * array.
 */
export default Ember.Mixin.create({

  _appController: Ember.computed.alias('controllers.application'),

  currentController: function () {
    var currentRouteName = this.get('currentRouteName');
    return this.controllerFor(currentRouteName);
  }.property('_appController.currentRouteName'),

  currentRouteName: function () {
    return this.get('_appController').get('currentRouteName');
  }.property('_appController.currentRouteName')

});
