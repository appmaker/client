import Errors from '../errors/errors';
import Ember from 'ember';
import Statistics from '../statistics/statistics';

var ErrorFactory = Ember.Object.create({
  create(error) {
    if (error instanceof Errors.AppMakerError) {
      return error;
    }

    return Errors.AppMakerError.create(error);
  }
});

/**
 * This mixin executes the <handleServerError> or <handleClientError> methods
 * once an error happens. The first one receives an object with the message and
 * status code and the last one only the message.
 */
export default Ember.Mixin.create({

  handleClientError(e) {
    if (e.name === 'TaskTimeoutError') {
      var conf = {
        template: 'modal',
        view: 'modal-task-timeout'
      };
      this.send('openModal', conf);
      this.send('statistics', new Statistics.Notification.Warn.Timeout());
    }
  },

  handleServerError(error) {
    if (error.status === 503) {
      this.send('openModal', {
        template: 'modal',
        view: 'modal-service-unavailable'
      });
    } else {
      this.get('handleError').call(this, error);
    }
  },

  handleError() {
    this.send('openModal', {
      template: 'modal',
      view: 'modal-unexpected-error'
    });
    this.send('statistics', new Statistics.Notification.Fail.UnExpected());
  },

  onError(error) {
    if (!error) {
      return;
    }

    // https://github.com/emberjs/ember.js/issues/5566
    if (error.name === 'TransitionAborted') {
      return;
    }

    var appMakerError = ErrorFactory.create(error);
    console.error(appMakerError.get('name'), error);
    appMakerError.handle(this);
  }

});
