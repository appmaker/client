import Ember from 'ember';

var ProjectItemMixin = Ember.Mixin.create({
  backgroundIcon: function() {
    var iconUrl = this.get('channel.appInfo.iconUrl');
    return ('background-image: url(' + iconUrl + ')').htmlSafe();
  }.property('channel.appInfo.iconUrl')
});

export default ProjectItemMixin;
