'use strict';

var _urq = _urq || [];

var UserReportManager = (function(doc) {

  var initialized = false;

  function getURL(url) {
    return doc.location.protocol + '//' + url;
  }

  return {
    init(config) {
      if (initialized) {
        return;
      }

      initialized = true;

      _urq.push(['initSite', config.code]);

      var ur = doc.createElement('script');
      ur.type = 'text/javascript';
      ur.async = true;
      ur.src = getURL(config.url);
      var s = doc.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ur, s);
    }
  }

}(document));
