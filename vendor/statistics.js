'use strict';

var StatisticsManager = (function() {

  var initialized = false;

  var enabled = true;

  function getURL(url) {
    return '//' + url;
  }

  return {
    init(config) {
      if (initialized) {
        return;
      }

      initialized = true;

      var name = 'ga';
      window['GoogleAnalyticsObject'] = name;

      window[name] = function() {
        (window[name].q = window[name].q || []).push(arguments)
      };

      window[name].l = 1 * new Date();

      var script = document.createElement('script');
      var firstScript = document.getElementsByTagName('script')[0];
      script.async = 1;
      script.src = getURL(config.url);
      firstScript.parentNode.insertBefore(script, firstScript);

      ga('create', config.code, 'auto');
      ga('send', 'pageview');
    },

    trackEvent(event) {
      if (!enabled) {
        return;
      }

      var category = event.get('category');
      var action = event.get('action');
      var label = event.get('label');

      ga('send', 'event', category, action, label);

      console.log('Category: ', category);
      console.log('Action: ', action);
      console.log('Label: ', label);
    },

    setState(value) {
      enabled = value;
    }
  }

}());
